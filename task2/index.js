const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;


const password = 'something';

app.get('/', (req, res) => {
    res.send('Hello everybody!');
});

app.get('/encode/:password', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(req.params.password))
});


app.get('/decode/:password', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(req.params.password));
});

app.listen(port, () => {
    console.log('We are on ' + port)
});